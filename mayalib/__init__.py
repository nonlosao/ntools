import time

import maya.cmds as cmds
import maya.OpenMaya as om


class UniqueNode(object):

    __slots__ = '__sel__', '__apiobj__', '__apifn__'

    def __init__(self, node):
        self.__sel__ = om.MSelectionList()
        self.__sel__.add(node)
        self.__apifn__ = None

    def __getAPIFn(self):
        # obj = om.MObject()
        # self.__sel.getDependNode(0, obj)
        return om.MFnDependencyNode(self.__apiobj__)


class BaseNode(str):

    def __init__(self, node):
        self.__node = UniqueNode(node)

        # t = time.time()
        # self.__uuid__ = cmds.ls(node, uuid=True)
        # self.__sel = om.MSelectionList()
        # sel = self.__sel
        # sel.clear()
        # sel.add(node)
        # obj = om.MObject()
        # sel.getDependNode(0, obj)
        # self.__apifn__ = None
        # self.__apifn__ = om.MFnDependencyNode(self.__apiobj__)
        # uuid_call_times.append(time.me()-t)
        # self.__uuid__ = self.__uuid__[0]

    @property
    def __node__(self):
        self.__apifn__ = self.__apifn__ or self.__getAPIFn()
        return self.__apifn__.name()
        # return self.__apifn__.fullPathName()
        sel = om.MSelectionList()
        uuid = om.MUuid(self.__uuid__)
        obj = om.MObject()
        dag = om.MDagPath()
        sel.add(uuid)
        sel.getDependNode(0, obj)
        om.MFnDagNode(obj).getPath(dag)
        return dag.fullPathName()

    def __getAPIFn(self):
        # obj = om.MObject()
        # self.__sel.getDependNode(0, obj)
        return om.MFnDependencyNode(self.__node.__apiobj__)

    def nodeName(self):
        return self.__str__()

    def __str__(self):
        return self.__node__.split("|")[-1]


class MonitorNode(BaseNode):

    def __init__(self, node):
        super(MonitorNode, self).__init__(node)


if __name__ == "__main__":
    sel = cmds.ls(sl=1)
    n = BaseNode(sel[0])
    cmds.rename(sel[0], "newName")
