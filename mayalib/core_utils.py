import pymel.core as pm


class IsolateDAG(object):
    """
    This context manager temporarily isolates the given DAG object. Its hierarchical position is restored on exit.
    """

    def __init__(self, dag, children=True, parent=False):
        """
        :param dag: the DAG node to isolate
        :param children: whether its children should be temporarily removed from the DAG node
        :param parent: whether the DAG node should be temporarily removed from its parent
        """
        self.dag = dag
        self.dagName = dag.nodeName()
        self.children = children
        self.parent = parent
        self.dagParent = self.dag.getParent()
        self.dagChildren = self.dag.getChildren(shapes=False)  # shapes=False untested
        # create a temp group to temporarily reparent DAG relatives
        self.tmpGrp = pm.group(name="tmp_grp", empty=True)
        if parent:
            pm.parent(self.dag, self.tmpGrp)
        if children and len(self.dagChildren):
            # children are renamed before reparenting to avoid unwanted conflicts
            for child in self.dagChildren:
                child.rename("_dagChild_" + child.nodeName())
            pm.parent(self.dagChildren, self.tmpGrp)

    def __enter__(self):
        return self.dag

    def __exit__(self, type, value, traceback):
        # reparent the children back under the DAG and renames them to their original name
        if self.children:
            for child in self.dagChildren:
                pm.parent(child, self.dag)
                child.rename(child.nodeName().replace("_dagChild_", ""))
        # reparent the DAG back under its parent
        if self.parent:
            if self.dagParent:
                pm.parent(self.dag, self.dagParent)
            else:
                pm.parent(self.dag, world=True)
            self.dag.rename(self.dagName)
        # remove the temp group
        pm.delete(self.tmpGrp)


class DependencyInfo(object):
    """
    This class represents a scene dependency.

    A scene dependency is any external file used/linked within a scene.
    It can be a reference, texture, edits, plugin, cache and so on.
    """
    # dependency types
    kUnknown = "unknown"
    kTexture = "texture"
    kReference = "reference"
    kEdit = "edit"
    kPlugin = "plugin"
    kCache = "cache"

    # dependency statuses
    kMissing = 0
    kFound = 1
    kPartial = 2

    def __init__(self):
        self.path = None
        self.plug = None
        self.status = None
        self.type = None

    def __str__(self):
        return "\nDependencyInfo\tplug: {0}, path: {1}".format(self.plug, self.path)
