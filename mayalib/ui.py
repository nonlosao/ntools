import shiboken2
from PySide2 import QtWidgets, QtCore, QtGui
from pymel import core as pm

from maya import OpenMayaUI as apiUI


def getMayaMainWindow():
    ptr = apiUI.MQtUtil.mainWindow()
    mayaWin = shiboken2.wrapInstance(long(ptr), QtWidgets.QWidget)
    return mayaWin


def toQtObject(mayaName):
    """
    Given the name of a Maya UI element of any type,
    return the corresponding QWidget or QAction.
    If the object does not exist, returns None
    """

    ptr = apiUI.MQtUtil.findControl(mayaName)
    if ptr is None:
        ptr = apiUI.MQtUtil.findLayout(mayaName)
    if ptr is None:
        ptr = apiUI.MQtUtil.findMenuItem(mayaName)
    if ptr is not None:
        return shiboken2.wrapInstance(long(ptr), QtWidgets.QWidget)


class ListKeyableAttributesWidget(QtWidgets.QListWidget):

    __name__ = "ListKeyableAttributesWidget"

    def __init__(self, parent=None):
        super(ListKeyableAttributesWidget, self).__init__(parent)
        self.setObjectName(self.__name__)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)

        self.scripJob = pm.scriptJob(event=("SelectionChanged", self.updateAttrs))

        self.destroyed.connect(lambda: self.deleteScriptjob(self.scripJob))

        self.updateAttrs()

    def updateAttrs(self):
        sel = pm.selected()
        attrLs = [attr.name() for obj in sel for attr in obj.listAttr(k=1)]

        self.clear()
        self.addItems(attrLs)

    @classmethod
    def deleteScriptjob(cls, sjid):
        pm.scriptJob(kill=sjid)


class ValueTracker(QtWidgets.QLabel):

    def __init__(self, attr, parent=None):
        super(ValueTracker, self).__init__(parent=parent)
        self.attr = attr
        # print "adding scriptjob for {}".format(attr)
        self.scriptJob = pm.scriptJob(attributeChange=(attr, self.updateAttrValue))
        self.updateAttrValue()
        self.destroyed.connect(lambda: self.deleteScriptJob(self.scriptJob))

    @classmethod
    def deleteScriptJob(cls, sjid):
        pm.scriptJob(kill=sjid)

    def updateAttrValue(self):
        self.setText("{:.3f}".format(self.attr.get()))


class Table(QtWidgets.QTableWidget):

    def __init__(self, hheaders):
        super(Table, self).__init__()
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.setColumnCount(len(hheaders))
        self.setRowCount(0)
        self.setHorizontalHeaderLabels(hheaders)
        self.horizontalHeader().setStretchLastSection(True)
        self.verticalHeader().setDefaultSectionSize(30)
        self.verticalHeader().hide()
        self.setShowGrid(False)

        self.rowBackgroundColorOpt1 = QtGui.QColor(45, 45, 45)
        self.rowBackgroundColorOpt2 = QtGui.QColor(60, 60, 60)
        self.currentRowBackgroundColor = self.rowBackgroundColorOpt1

        self.scriptJobs = []
        self.destroyed.connect(self.deleteScriptJobs)

    def toggleRowColor(self):
        if self.currentRowBackgroundColor is self.rowBackgroundColorOpt1:
            self.currentRowBackgroundColor = self.rowBackgroundColorOpt2
        else:
            self.currentRowBackgroundColor = self.rowBackgroundColorOpt1

    def clearContents(self, *args, **kwargs):
        self.blockSignals(True)
        self.currentRowBackgroundColor = self.rowBackgroundColorOpt1
        # kill all the script jobs and reset the list
        self.deleteScriptJobs()
        super(Table, self).clearContents(*args, **kwargs)
        self.blockSignals(False)

    def deselect(self):
        ranges = self.selectedRanges()
        for r in ranges:
            self.setRangeSelected(r, False)

    def selectByText(self, text, row=None, column=None):
        print "searching for text: ", text
        rowArgs = (row, row+1) if row is not None else (0, self.rowCount())
        columnArgs = (column, column+1) if column is not None else (0, self.columnCount())
        for r in range(*rowArgs):
            for c in range(*columnArgs):
                item = self.item(r, c)
                if item.text() == text:
                    print("Selected cell ", text)
                    self.deselect()
                    self.setCurrentCell(r, c)
                    return

    def addScriptJob(self, event, function):
        self.scriptJobs.append(pm.scriptJob(event=(event, function)))

    def deleteScriptJobs(self):
        if self.scriptJobs:
            pm.scriptJob(kill=self.scriptJobs)
        self.scriptJobs = []

    def addRow(self, items):
        if len(items) > self.columnCount():
            raise ValueError("Items are more than the number of columns in the table")
        rowCount = self.rowCount()
        self.setRowCount(rowCount + 1)
        for index, item in enumerate(items):
            if isinstance(item, QtWidgets.QTableWidgetItem):
                item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                item.setBackgroundColor(self.currentRowBackgroundColor)
                self.setItem(rowCount, index, item)
            elif isinstance(item, QtWidgets.QWidget):
                item.setAutoFillBackground(True)
                item.setAttribute(QtCore.Qt.WA_StyledBackground, True)
                item.setStyleSheet('background-color: {};'.format(self.currentRowBackgroundColor.name()))
                # palette = item.palette()
                # palette.setColor(item.backgroundRole(), self.currentRowBackgroundColor)
                # item.setPalette(palette)
                self.setCellWidget(rowCount, index, item)
            else:
                raise TypeError("Object type not supported. Can only take Widgets or TableWidgetItems")

    # def edit(self, index, trigger, event):
    #     if trigger == QtWidgets.QAbstractItemView.DoubleClicked:
    #         # DoubleClick Killed!
    #         return False
    #     return QtWidgets.QTableWidget.edit(self, index, trigger, event)
