import pymel.core as pm
from core_utils import IsolateDAG


def getCleanChain(first, last):
    """
    Verify that the given joints belong to the same hierarchy, and return a list
    containing all the joints that belong to the chain, ordered from first to last
    :param first: first joint of the chain
    :param last:  last joint of the chain
    :return: a list containing all the joints of the chain
    """
    # check if chain selection is valid
    if last not in first.getChildren(ad=True):
        pm.error("'Joints do not belong to the same chain, or were not selected in the right order.")
    # get clean chain from first to last (in case of diverging chains)
    chain = [last]
    while True:
        chain.insert(0, chain[0].getParent())
        if chain[0] == first:
            break
    return chain


def orientChain(first, last, primaryAxis="x", secondaryAxis="y"):
    """
    Orient all the joints in the chain, try to align the secondary axis with the plane on which the chain lays
    :param first: first joint of the chain
    :param last: last joint of the chain
    :param primaryAxis: primary axis used to orient the joints
    :param secondaryAxis: secondary axis used to orient the joints
    """
    axes = {"x": (1, 0, 0), "y": (0, 1, 0), "z": (0, 0, 1), "-x": (-1, 0, 0), "-y": (0, -1, 0), "-z": (0, 0, -1)}
    chain = getCleanChain(first, last)
    # figure out what to align the secondary axis to
    firstToLast = (chain[-1].getTranslation(space="world") - chain[0].getTranslation(space="world")).normal()
    firstToSecond = (chain[1].getTranslation(space="world") - chain[0].getTranslation(space="world")).normal()
    alignVector = (firstToSecond - firstToLast).normal()
    # automatically align joints
    for i, jnt in enumerate(chain):
        with IsolateDAG(jnt) as jnt:
            # reset values on original joint
            jnt.r.set([0, 0, 0])
            jnt.ra.set([0, 0, 0])
            jnt.jo.set([0, 0, 0])
            if i + 1 != len(chain):
                pm.delete(
                    pm.aimConstraint(chain[i + 1], jnt, aim=axes[primaryAxis], u=axes[secondaryAxis], wut="vector",
                                     wu=alignVector, mo=False))


def alignJointChainOnPlane(first, last, plane, autoOrient=("x", "y")):
    """
    Snap all the joints to the closest point on the given plane
    :param first: first joint of the chain
    :param last: last joint of the chain
    :param plane: plane on which the joints should lay
    :param autoOrient: try to orient the joints so that the secondary axis lays on the plane
    """
    chain = getCleanChain(first, last)
    plane = pm.duplicate(plane)[0]
    pm.makeIdentity(plane, apply=True)
    for jnt in chain:
        pos = plane.getClosestPoint(jnt.getTranslation(space="world"))
        with IsolateDAG(jnt) as j:
            j.setTranslation(pos[0], space="world")
    pm.delete(plane)
    if autoOrient:
        orientChain(first, last, *autoOrient)


def bakeJointOrientationToJointOrient(jnt):
    """
    Bake the orientation of the joint to the JointOrient attribute, therefore leaving the rotation channels clean.
    :param jnt: the joint for which to bake the orientation
    """
    if jnt.nodeType() != "joint":
        print("Skipping {}. Not a joint.".format(jnt))
        return
    sel = pm.selected()
    with IsolateDAG(jnt, parent=True) as jnt:
        dup = pm.duplicate(jnt, po=True)[0]
        pm.parent(jnt, dup)
        pm.makeIdentity(jnt, apply=True)
    pm.select(sel)


def bakeJointOrientationToRot(jnt):
    """
    Bake the orientation of the joint to the Rotate attribute, therefore leaving the JointOrient channels clean.
    :param jnt: the joint for which to bake the orientation
    """
    if jnt.nodeType() != "joint":
        print("Skipping {}. Not a joint.".format(jnt))
        return
    sel = pm.selected()
    with IsolateDAG(jnt) as jnt:
        dup = pm.duplicate(jnt, po=True)[0]
        pm.makeIdentity(jnt, jo=True, apply=True)
        pm.matchTransform(jnt, dup)
        pm.delete(dup)
    pm.select(sel)


def addLeafRollBones(jnt, count=3, primaryAxis="x"):
    """
    Add leaf roll bones to the selected joint.
    :param jnt: the joint on which to add the roll bones
    :param count: number of roll bones to add
    :param primaryAxis: axis on which to add the bones
    """
    distance = jnt.listRelatives(c=True, type="joint")[0].attr("t" + primaryAxis).get() / (count + 1)
    for n in range(count):
        pm.select(jnt, r=True)
        roll = pm.joint(name=jnt.nodeName() + "Roll{}".format(n + 1))
        roll.attr("t" + primaryAxis).set(distance * (n + 1))
        roll.radius.set(3)


def rotateOrientAround(jnt, axis="x", amount=90):
    """
    Rotate the orientation of a joint around the specified axis. Used to quickly adjust or offset a joint orient
    :param jnt: the joint for which to change the orientation
    :param axis: the axis around which to rotate the orientation
    :param amount: how much to rotate the orientation
    """
    with IsolateDAG(jnt, parent=True) as jnt:
        dup = pm.duplicate(jnt)[0]
        pm.parent(jnt, dup)
        pm.rotate(jnt, amount, r=True, os=True, **{axis: True})
    pm.select(jnt)


def normalizeRotations(jnt):
    """
    Normalize the rotations so the values do not exceed 180 degrees
    :param jnt: the joint for which to normalize the rotations
    """
    for a in ["rx", "ry", "rz", "jox", "joy", "joz", "raz", "ray", "raz"]:
        attr = jnt.attr(a)
        value = attr.get() % 360
        avalue = abs(value)
        value = (avalue - 360) * (avalue / value) if avalue > 180 else value
        attr.set(value)
        # example: 200 = -160 | -200 = 160 | 120 = 120 | 380 = 20
