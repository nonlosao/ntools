import os, time
from PySide2 import QtWidgets
import pymel.core as pm
from mad.mayalib.ui import get_maya_window


def delete_anim_lyrs():
    """
    Delete all the animation layers in the current scene
    """
    animLyrLs = pm.ls(type="animLayer")
    for lyr in animLyrLs:
        if pm.objExists(lyr):
            pm.delete(lyr)
    animLyrLeft = pm.ls(type="animLayer")
    diff = set(animLyrLs) - set(animLyrLeft)
    print("    deleted {} anim layers. {} left".format(len(diff), len(diff)))


def delete_namespaces():
    """
    Delete all the namespaces in the current scene and merge them with the root namespace
    """
    defaults = ['UI', 'shared']
    namespaces = (ns for ns in pm.namespaceInfo(lon=True) if ns not in defaults)
    namespaces = (pm.Namespace(ns) for ns in namespaces)
    for ns in namespaces:
        print("Removing namespace {}".format(ns))
        tmpName = "__tempNS__"
        pm.namespace(rename=(ns.shortName(), tmpName))
        ns = pm.Namespace(tmpName)
        ns.move(":", 1)
        ns.remove()


def import_reference():
    """
    Import all the references in the current scene
    """
    for ref in pm.listReferences():
        ref.importContents(removeNamespace=True)


def normalize_skinning():
    # TODO for each skin cluster/mesh in the scene, normalize weights
    pass


def optimize_scene():
    ui_node_types = ["nodeGraphEditorInfo", "hyperGraphInfo", "hyperLayout", "sequenceManager", "sequencer",
                     "shapeEditorManager", "strokeGlobals", "renderLayerManager", "renderGlobalsList",
                     "poseInterpolatorManager"]
    config_nodes = ["sceneConfigurationScriptNode", "uiConfigurationScriptNode"]
    nodes = [n for t in ui_node_types for n in pm.ls(type=t)]
    nodes += [n for t in config_nodes for n in sorted(pm.ls(t + "*"))[1:]]
    for n in nodes:
        pm.delete(n)
    pm.optionVar["nurbsSrfOption"] = 0
    os.environ["MAYA_TESTING_CLEANUP"] = "1"
    pm.mel.cleanUpScene(1)
    pm.mel.performCleanUpScene()
    del os.environ["MAYA_TESTING_CLEANUP"]
    # pm.mel.cleanUpScene(3)


def delete_tweaks():
    """Delete all tweak nodes from the scene"""
    pm.delete(pm.ls(type="tweak"))


def unlockNormals(meshLs=None):
    """
    Unlock the normals on given meshes.
    If the meshes have const. history or have deformers attached, then act on the OrigShape and keep history clean.
    :param meshLs: list of meshes on which to unlock normals. If nothing is passed, process all meshes in the scene.
    """
    print("\nUnlocking normals\n--------------------")
    pm.delete(pm.ls(type="tweak"))  # tweaks might store locked normals info
    meshLs = meshLs or pm.ls(type="mesh")
    for mesh in meshLs:
        if bool(mesh.inMesh.inputs()):  # if mesh is driven/deformed, then skip it
            continue

        isInterm = mesh.isIntermediate()
        mesh.setIntermediate(False)
        pm.polyNormalPerVertex(mesh, ufn=True)
        pm.polySoftEdge(mesh, a=180, ch=0)

        pm.delete(mesh, ch=True)
        mesh.setIntermediate(isInterm)


def rig_cleanup():
    print("\nRig Cleanup\n--------------------")
    import_reference()
    delete_namespaces()
    delete_anim_lyrs()
    normalize_skinning()
    delete_tweaks()
    optimize_scene()


if __name__ == "__main__":
    unlockNormals()
    rig_cleanup()

