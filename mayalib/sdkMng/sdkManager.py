import pymel.core as pm
from PySide2 import QtCore, QtWidgets, QtGui

from maya.utils import processIdleEvents

import mayalib.ui as mayaui
reload(mayaui)


# FIND CONNECTED SDKs AND DISPLAY THEM
# this "skips" the unitConversion nodes
# it does not check whether the drivers are themselves driven

# TODO make the UI respond to changes such as added, deleted or updated keyframes or driven attributes
# TODO add warnings on driven attributes that don't have values on all keyframes
# FIXME even if editing of tableItems is disabled, they still get renamed if typing when selected

# update UI if driven attr is removed (monitor attr)
# update UI if pose is removed (monitor all SDK for driver)

# ------ UI ------ #

class SDKManager(QtWidgets.QDialog):

    __name__ = "SDKManager"

    def __init__(self, parent):
        super(SDKManager, self).__init__(parent)
        self.setWindowTitle("SDK Manager")
        self.setObjectName(self.__name__)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.warningIcon = self.style().standardIcon(QtWidgets.QStyle.SP_MessageBoxWarning)

        self.metanode = self.getMetanode()

        self.scriptJobs = []
        self.scriptJobs.append(pm.scriptJob(con=(self.metanode.drivers, self.populate)))
        self.scriptJobs.append(pm.scriptJob(event=("Undo", self.onUndo)))
        self.scriptJobs.append(pm.scriptJob(event=("SceneOpened", self.onSceneChange)))

        self.driverFont = QtGui.QFont("Segoe UI", 10)
        self.drivenFont = QtGui.QFont("Segoe UI", 8)

        self.splitView = QtWidgets.QSplitter()

        # drivers
        self.driverTable = mayaui.Table(["Drivers", "Value"])
        self.driverPanel = QtWidgets.QWidget()
        self.addDriverBtn = QtWidgets.QPushButton("+")
        self.delDriverBtn = QtWidgets.QPushButton("-")
        self.setupDriverListContextualMenu()

        # keyframes
        self.poseTable = mayaui.Table(["Poses"])
        self.posePanel = QtWidgets.QWidget()
        self.addPoseBtn = QtWidgets.QPushButton("+")
        self.delPoseBtn = QtWidgets.QPushButton("-")

        # driven
        self.drivenTable = mayaui.Table(["Driven", "Value"])
        self.drivenPanel = QtWidgets.QWidget()
        self.addDrivenBtn = QtWidgets.QPushButton("+")
        self.delDrivenBtn = QtWidgets.QPushButton("-")
        self.setupDrivenListContextualMenu()

        # create graph editor
        self.selcon = pm.selectionConnection()
        # frameLayout = pm.frameLayout()
        # frameLayoutQt = mayaui.toQtObject(frameLayout)
        # graph editor, temp parent to maya wnd, constraint only vertical drags (value only, no time)
        self.graphEditor = pm.animCurveEditor(mlc=self.selcon, parent=parent.objectName(), constrainDrag=2)
        self.graphEditorQt = mayaui.toQtObject(self.graphEditor)

        self.createLayout()
        self.findExistingSDKs()
        self.connectSignals()
        self.populate()

        # if self.driverTable.rowCount():
        #     self.driverTable.setCurrentCell(0, 0)
        #     self.onDriverClicked()

        self.resize(1200, 500)
        # self.deleteLater()

    def createLayout(self):
        # driver
        btnLayout = QtWidgets.QHBoxLayout()
        # btnLayout.setContentsMargins(10,0,10,0)
        btnLayout.addWidget(self.addDriverBtn)
        btnLayout.addWidget(self.delDriverBtn)
        driverLayout = QtWidgets.QVBoxLayout()
        driverLayout.setContentsMargins(0,0,0,0)
        driverLayout.addLayout(btnLayout)
        driverLayout.addWidget(self.driverTable)
        self.driverTable.horizontalHeader().setStretchLastSection(False)
        self.driverTable.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.driverTable.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.Fixed)
        self.driverTable.horizontalHeader().resizeSection(1, 60)
        self.driverPanel.setLayout(driverLayout)

        # keyframes
        btnLayout = QtWidgets.QHBoxLayout()
        btnLayout.addWidget(self.addPoseBtn)
        btnLayout.addWidget(self.delPoseBtn)
        keyframesLayout = QtWidgets.QVBoxLayout()
        keyframesLayout.setContentsMargins(0,0,0,0)
        keyframesLayout.addLayout(btnLayout)
        keyframesLayout.addWidget(self.poseTable)
        self.posePanel.setLayout(keyframesLayout)

        # driven
        btnLayout = QtWidgets.QHBoxLayout()
        btnLayout.addWidget(self.addDrivenBtn)
        btnLayout.addWidget(self.delDrivenBtn)
        drivenLayout = QtWidgets.QVBoxLayout()
        drivenLayout.setContentsMargins(0,0,0,0)
        drivenLayout.addLayout(btnLayout)
        drivenLayout.addWidget(self.drivenTable)
        self.drivenTable.horizontalHeader().setStretchLastSection(False)
        self.drivenTable.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.drivenTable.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.Fixed)
        self.drivenTable.horizontalHeader().resizeSection(1, 60)
        self.drivenPanel.setLayout(drivenLayout)

        self.splitView.addWidget(self.driverPanel)
        self.splitView.addWidget(self.posePanel)
        self.splitView.addWidget(self.drivenPanel)
        self.splitView.addWidget(self.graphEditorQt)
        self.splitView.setSizes([300, 150, 300, 800])

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.splitView)
        self.setLayout(layout)

    def connectSignals(self):
        self.driverTable.currentCellChanged.connect(self.onDriverClicked)
        self.poseTable.cellDoubleClicked.connect(self.onPoseDoubleClicked)
        self.drivenTable.currentCellChanged.connect(self.onDrivenCellChanged)
        self.addDriverBtn.clicked.connect(self.onAddDriverBtnClicked)
        self.delDriverBtn.clicked.connect(self.onDelDriverBtnClicked)
        self.addPoseBtn.clicked.connect(self.onAddPoseBtnClicked)
        self.delPoseBtn.clicked.connect(self.onDelPoseBtnClicked)
        self.addDrivenBtn.clicked.connect(self.onAddDrivenBtnClicked)
        self.delDrivenBtn.clicked.connect(self.onDelDrivenBtnClicked)
        self.destroyed.connect(self.closeEvent)

    def findExistingSDKs(self):
        driverLs = self.metanode.drivers.inputs(plugs=True)
        for sdk in getSDKs():
            exists = False
            sdkDriver = sdk.getDriver()
            if sdkDriver in driverLs:
                exists = True
            if exists:
                continue
            self.addDriver(sdkDriver)

    def populate(self):
        self.driverTable.clearContents()
        self.driverTable.setRowCount(0)
        self.metanode = self.getMetanode()
        previousDriverNode = None
        for driver in sorted(self.metanode.drivers.inputs(plugs=True), key=lambda a: a.name()):

            if previousDriverNode and previousDriverNode != driver.node():
                self.driverTable.toggleRowColor()
            previousDriverNode = driver.node()

            driverItem = QtWidgets.QTableWidgetItem()
            driverItem.setText(driver.name())
            # driverItem.setData(QtCore.Qt.UserRole, driver)
            if isDriverAlreadyDriven(driver):
                driverItem.setData(QtCore.Qt.DecorationRole, self.warningIcon)
                driverItem.setData(QtCore.Qt.ToolTipRole,
                                   "This driver is already driven. The 'Go to pose' tool will be disabled")
            valueItem = mayaui.ValueTracker(driver)
            self.driverTable.addRow([driverItem, valueItem])

        # select the first driver
        if self.driverTable.rowCount():
            self.driverTable.setCurrentCell(0, 0)
            self.onDriverClicked()
        else:
            self.poseTable.clearContents()
            self.drivenTable.clearContents()
            pm.selectionConnection(self.selcon, e=True, clear=True)

    def onDriverClicked(self):
        driver = self.getCurrentDriver()

        # populate driven list
        self.drivenTable.clearContents()
        self.drivenTable.setRowCount(0)
        previousDrivenNode = None
        for driven in getDriven(driver):
            # if the item relates to a new node, change bg color
            if previousDrivenNode and previousDrivenNode != driven.node():
                self.drivenTable.toggleRowColor()
            previousDrivenNode = driven.node()

            # add refresh callback for each sdk
            # sdk = driven.inputs()[0]
            # self.drivenTable.addScriptJob()

            # add items
            attrItem = QtWidgets.QTableWidgetItem()
            attrItem.setText(str(driven))
            valueItem = mayaui.ValueTracker(driven)
            self.drivenTable.addRow([attrItem, valueItem])

        isDriverDriven = isDriverAlreadyDriven(driver)
        if isDriverDriven:
            try:
                self.poseTable.cellDoubleClicked.disconnect()
            except RuntimeError:
                pass
        else:
            try:
                self.poseTable.cellDoubleClicked.connect(self.onPoseDoubleClicked)
            except RuntimeError:
                pass

        # populate pose list
        self.poseTable.clearContents()
        self.poseTable.setRowCount(0)
        itemDisabledColor = QtGui.QColor(100, 100, 100)
        for pose in getPoses(driver):
            poseItem = QtWidgets.QTableWidgetItem()
            poseItem.setText("{:.3f}".format(pose))
            poseItem.setData(QtCore.Qt.ToolTipRole, pose)  # actual floating point value
            if isDriverDriven:  # disable the item (just a visual hint)
                poseItem.setTextColor(itemDisabledColor)
            self.poseTable.addRow([poseItem])

        if self.poseTable.rowCount():
            self.poseTable.setCurrentCell(0, 0)
        if self.drivenTable.rowCount():
            self.drivenTable.setCurrentCell(0, 0)
        else:
            pm.selectionConnection(self.selcon, e=True, clear=True)
        # self.drivenList.itemClicked.emit()

    def onDrivenCellChanged(self, currentRow, currentColumn, previousRow, previousColumn):
        if currentRow == -1:  # if list is cleared, selection changed signal is triggered, row will be -1
            return
        drivenItem = self.getCurrentDriven()
        sdk = drivenItem.inputs()[0]
        pm.selectionConnection(self.selcon, e=True, clear=True)
        pm.selectionConnection(self.selcon, e=True, select=sdk)
        pm.animCurveEditor(self.graphEditor, e=1, autoFit="on", autoFitTime="on", snapTime="keyframe")

    def onPoseDoubleClicked(self, row=None, column=None):
        driver = self.getCurrentDriver()
        item = self.poseTable.currentItem()
        value = item.data(QtCore.Qt.ToolTipRole)
        driver.set(value)

    def onAddDriverBtnClicked(self):
        sel = pm.selected()
        if len(sel) != 1 or pm.nodeType(sel[0]) not in ["transform", "joint"]:
            pm.informBox("Warning", "Please select exactly one transform.", "Close")
            return
        driver = GetKeyableAttributesUI.getDriver()
        self.addDriver(driver)
        self.populate()
        self.driverTable.setCurrentCell(self.driverTable.rowCount()-1, 0)

    def onDelDriverBtnClicked(self):
        driver = self.driverTable.currentItem()
        msg = "Are you sure that you want to delete the driver {} and all its SDKs?".format(driver.text())
        ret = pm.confirmBox("Delete Driver?", msg, "Continue", "Cancel")
        if not ret:
            return
        sdks = getSDKs(driver=driver.text())
        if sdks:
            pm.delete(sdks)
        pm.Attribute(driver.text()).disconnect(self.metanode.drivers, nextAvailable=True)
        self.populate()

    def onAddPoseBtnClicked(self):
        if not self.poseTable.rowCount():
            driven = GetKeyableAttributesUI.getSingleDriven()
            if not driven:
                return
            sdk = SDK.new(self.getCurrentDriver(), driven)
        addCurrentPose(self.getCurrentDriver())
        self.onDriverClicked()

    def onDelPoseBtnClicked(self):
        if not pm.confirmBox("Delete Pose",
                             "Are you sure you want to delete the selected pose from all the driven objects?"):
            return

        driver = self.getCurrentDriver()
        for sdk in getSDKs(driver=driver):
            sdk.delKeyframe(self.getCurrentPose())
        self.onDriverClicked()

    def onAddDrivenBtnClicked(self):
        sel = pm.selected()
        if not sel:
            pm.informBox("Warning", "You must select at least one node with keyable attributes.", "Close")
            return
        driver = self.getCurrentDriver()
        drivenAttrs = GetKeyableAttributesUI.getMultiDriven()
        if not drivenAttrs:
            return 
        for attr in drivenAttrs:
            for keyframe in getPoses(driver=driver):
                pm.setDrivenKeyframe(attr, currentDriver=driver, driverValue=keyframe)
        self.onDriverClicked()

    def onDelDrivenBtnClicked(self):
        driverItem = self.driverTable.currentItem()
        drivenItems = self.drivenTable.selectedItems()
        formattedItems = "\n    * ".join([""]+[d.text() for d in drivenItems])
        msg = "Are you sure that you want to delete the SDKs for the driven attributes?\n{}".format(formattedItems)
        ret = pm.confirmBox("Delete Driven?", msg, "Continue", "Cancel")
        if not ret:
            return
        for drivenItem in drivenItems:
            sdks = getSDKs(driven=drivenItem.text())
            if sdks:
                pm.delete(sdks)
        self.onDriverClicked()

    def setupDriverListContextualMenu(self):
        def cme(e):
            selectDriverAction = QtWidgets.QAction(self.driverTable)
            selectDriverAction.setText("Select Driver Object")
            selectDriverAction.triggered.connect(
                lambda: pm.select(self.getCurrentDriver(node=True)))
            menu = QtWidgets.QMenu(self.driverTable)
            menu.addAction(selectDriverAction)
            menu.popup(QtGui.QCursor.pos())
        self.driverTable.contextMenuEvent = cme

    def setupDrivenListContextualMenu(self):
        def updateDrivenValueOnSelected():
            driver = self.getCurrentDriver()
            keyframe = self.getCurrentPose()
            if driver.get() != keyframe:

                # display dialog with multiple choices to handle the situations
                closestPose = getDriverClosestPose(driver, getPoses(driver=driver))
                title = "Driver not in position"
                msg = "The Driver is currently not in the selected Pose." \
                      "\n\nYou can either" \
                      "\n\t * Snap it to the closest pose: [{}]" \
                      "\n\t * Go to the currently selected pose: [{}]" \
                      "\n\t * Add a new pose at current: [{}]".format(closestPose, keyframe, driver.get())
                goToClosestBtn = "Go to closest Pose"
                goToSelectedBtn = "Go to selected Pose"
                addBtn = "Add current Pose"
                choice = pm.confirmBox(title, msg, goToClosestBtn, goToSelectedBtn, addBtn, "Cancel")

                drivenLs = self.getSelectedDriven()

                if choice == goToClosestBtn:
                    for driven in drivenLs:
                        SDK(driven.inputs()[0]).addKeyframe(time=closestPose, value=driven.get())
                    driver.set(closestPose)
                    for row in range(self.poseTable.rowCount()):
                        if closestPose == self.poseTable.item(row, 0).data(QtCore.Qt.ToolTipRole):
                            self.poseTable.setCurrentCell(row, 0)
                            break
                elif choice == goToSelectedBtn:
                    for driven in drivenLs:
                        SDK(driven.inputs()[0]).addKeyframe(time=self.getCurrentPose(), value=driven.get())
                    driver.set(self.getCurrentPose())
                elif choice == addBtn:
                    addCurrentPose(driver)
                    self.onDriverClicked()
                    return
                else:
                    return
            addCurrentPose(driver)

        def cme(e):
            selectDrivenAction = QtWidgets.QAction(self.driverTable)
            selectDrivenAction.setText("Select Driven Object")
            selectDrivenAction.triggered.connect(
                lambda: pm.select(self.getSelectedDriven(node=True)))
            updateDrivenAction = QtWidgets.QAction(self.drivenTable)
            updateDrivenAction.setText("Update SDK with current driven value")
            updateDrivenAction.triggered.connect(updateDrivenValueOnSelected)
            menu = QtWidgets.QMenu(self.driverTable)
            menu.addAction(selectDrivenAction)
            menu.addAction(updateDrivenAction)
            menu.popup(QtGui.QCursor.pos())
        self.drivenTable.contextMenuEvent = cme

    def onUndo(self):
        # get selections for each table
        driver = self.getCurrentDriver()
        pose = self.getCurrentPose()
        driven = self.getCurrentDriven()

        self.populate()

        if driver:
            self.driverTable.selectByText(driver.name(), column=0)
        if driven:
            self.drivenTable.selectByText(driven.name(), column=0)
        if pose:
            self.poseTable.selectByText("{:.3f}".format(pose), column=0)

    def onSceneChange(self):
        processIdleEvents()
        self.metanode = self.getMetanode()
        self.findExistingSDKs()
        self.populate()

    def addDriver(self, driver):
        """For each SDK found, connect it to the meta node, then populate the table"""
        if not driver:
            return
        driver = pm.Attribute(driver)
        if driver not in self.metanode.drivers.inputs(plugs=True):
            driver.connect(self.metanode.drivers, nextAvailable=True)

    def getCurrentDriver(self, node=False):
        item = self.driverTable.currentItem()
        if item is None:
            return None
        driver = pm.PyNode(item.text())
        if node:
            return driver.node()
        return driver

    def getCurrentPose(self):
        item = self.poseTable.currentItem()
        value = item.data(QtCore.Qt.ToolTipRole)
        return value

    def getCurrentDriven(self, node=False):
        item = self.drivenTable.currentItem()
        if item is None:
            return None
        driven = pm.PyNode(item.text())
        if node:
            return driven.node()
        return driven

    def getSelectedDriven(self, node=False):
        sel = [pm.PyNode(n.text()) for n in self.drivenTable.selectedItems() or [self.drivenTable.currentItem()]]
        if node:
            sel = [s.node() for s in sel]
        return sel

    def closeEvent(self, event=None):
        self.onDestroy()
        # print "closing SDK manager"
        # super(SDKManager, self).closeEvent(event)
        # self.setParent(None)
        # self.deleteLater()

    def onDestroy(self):
        pm.scriptJob(kill=self.scriptJobs)
        if self.__name__ in pm.lsUI():
            pm.deleteUI(self.__name__)
        # self.setParent(None)
        # self.deleteLater()

    def getMetanode(self):
        matchingNodes = pm.ls(self.__name__, type="network")
        if not len(matchingNodes):
            node = pm.createNode("network", name=self.__name__)
            node.addAttr("drivers", at="message", multi=True, indexMatters=False)
        else:
            node = matchingNodes[0]
        return node


class GetKeyableAttributesUI(QtWidgets.QDialog):

    __name__ = "GetKeyableAttributesUI"

    def __init__(self, parent=None):
        super(GetKeyableAttributesUI, self).__init__(parent)
        self.setObjectName(self.__name__)

        self.attrList = mayaui.ListKeyableAttributesWidget()
        self.addBtn = QtWidgets.QPushButton("Add")
        self.cancelBtn = QtWidgets.QPushButton("Cancel")

        self.setLayout(QtWidgets.QVBoxLayout())
        btnLayout = QtWidgets.QHBoxLayout()
        btnLayout.addWidget(self.addBtn)
        btnLayout.addWidget(self.cancelBtn)
        self.layout().addWidget(self.attrList)
        self.layout().addLayout(btnLayout)

        self.addBtn.clicked.connect(self.accept)
        self.cancelBtn.clicked.connect(self.close)
        self.destroyed.connect(self.onDestroy)

    def onDestroy(self):
        print("Actually Destroying UI")

    def closeEvent(self, event=None):
        if self.__name__ in pm.lsUI():
            print("Deleting UI {}".format(self.__name__))
            pm.deleteUI(self.__name__)
        self.setParent(None)
        self.deleteLater()

    @classmethod
    def getDriver(cls):
        ui = cls()
        ui.setWindowTitle("Add Driver Attribute")
        if ui.exec_() != QtWidgets.QDialog.Accepted:
            ui.close()
            return None
        ui.close()
        return pm.Attribute(ui.attrList.currentItem().text())

    @classmethod
    def getMultiDriven(cls):
        ui = cls()
        ui.setWindowTitle("Add Driven Attributes")
        ui.attrList.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        if ui.exec_() != QtWidgets.QDialog.Accepted:
            ui.close()
            return None
        ui.close()
        return [pm.Attribute(item.text()) for item in ui.attrList.selectedItems()]\

    @classmethod
    def getSingleDriven(cls):
        ui = cls()
        ui.setWindowTitle("Add Driven Attribute")
        if ui.exec_() != QtWidgets.QDialog.Accepted:
            ui.close()
            return None
        ui.close()
        return pm.Attribute(ui.attrList.currentItem().text())


class SDK(object):

    def __init__(self, node):
        self.__node__ = pm.PyNode(node)

    def __str__(self):
        return self.__node__.nodeName()

    def __repr__(self):
        return self.__str__()

    @classmethod
    def new(cls, driver, driven):
        pm.setDrivenKeyframe(driven, currentDriver=driver, driverValue=driver.get())
        sdk = getSDKs(driven=driven)[0]
        return sdk

    def getDriver(self):
        drivers = self.__node__.input.inputs(plugs=True)
        if not drivers:
            return None
        driver = drivers[0]
        while driver is not None and driver.nodeType() == "unitConversion":
            driver = self._parse_unitconversion(driver.node())
        return driver

    def getDriven(self):
        return self.__node__.output.outputs(plugs=True)

    def getKeyframes(self):
        kfr = []
        for d in self.getDriven():
            kfr.extend([self.__node__.getUnitlessInput(i) for i in range(self.__node__.numKeys())])
        return sorted(list(set(kfr)))

    def addKeyframe(self, time=None, value=None):
        time = time or self.getDriver().get()
        value = value or self.getDriven()[0].get()
        pm.setKeyframe(self.__node__, float=time, value=value)

    def delKeyframe(self, time):
        pm.cutKey(self.__node__, float=[float(time)], clear=True)

    def _parse_unitconversion(self, node):
        input_ls = node.input.inputs(plugs=True)
        if not input_ls:
            return None
        input = input_ls[0]
        return input


def showUI():
    if pm.window(SDKManager.__name__, exists=True):
        pm.deleteUI(SDKManager.__name__)
    ui = SDKManager(mayaui.getMayaMainWindow())
    ui.show()
    # print SCRIPT_JOBS


# ------ CTRL ------ #


def getSDKs(driver=None, driven=None):
    sdkLs = [SDK(sdk) for sdk in pm.ls(type=["animCurveUA", "animCurveUL", "animCurveUU"])]

    filteredList = []
    for sdk in sdkLs:
        sdkDriver = sdk.getDriver()
        if driver and driver != sdkDriver:
            continue
        sdkDriven = sdk.getDriven()
        if driven and driven not in sdkDriven:
            continue
        filteredList.append(sdk)
    return filteredList


def getPoses(driver=None, driven=None):
    poses = []
    for sdk in getSDKs(driver=driver, driven=driven):
        poses.extend(sdk.getKeyframes())
    return sorted(list(set(poses)))


def getDriven(driver):
    return sorted([d for sdk in getSDKs(driver=driver) for d in sdk.getDriven()])


def updateDrivenValueOnSdk(driven, value=None):
    sdk = SDK(driven.inputs()[0])
    value = value or driven.get()
    driver = sdk.getDriver()
    pm.setKeyframe(sdk, float=driver.get(), value=value)


def addCurrentPose(driver):
    sdkLs = getSDKs(driver=driver)
    for sdk in sdkLs:
        sdk.addKeyframe()


def getDriverClosestPose(driver, poses):
    value = driver.get()
    return poses[min(range(len(poses)), key=lambda i: abs(poses[i] - value))]


def isDriverAlreadyDriven(attribute):
    inputs = attribute.inputs()
    if inputs:
        if any(map(lambda n: "animCurveT" in pm.nodeType(n), inputs)):
            return False
        return True
    if pm.nodeType(attribute.node()) == "joint" and isDrivenByIk(attribute.node()):
        return True
    return False


def isDrivenByIk(joint):
    # find effectors
    effectors = joint.listRelatives(ad=True, type="ikEffector")
    for effector in effectors:
        handles = effector.handlePath.outputs()
        for handle in handles:
            startJoints = handle.startJoint.inputs()
            for startJoint in startJoints:
                if joint in startJoint.listRelatives(ad=True, type="joint"):
                    return True
    return False


if __name__ == "__main__":
    showUI()
