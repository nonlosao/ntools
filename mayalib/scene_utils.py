import shutil
import os
import pymel.core as pm
from core_utils import DependencyInfo


def getFileDependencies():
    """
    Return a list of all the dependencies in the scene
    :return: dependencies list
    :rtype: list of DependencyInfo
    """
    dependency_ls = []
    pm.filePathEditor(refresh=1)
    dependency_dir_ls = pm.filePathEditor(q=True, listDirectories="")
    if not dependency_dir_ls:
        print "No dependencies found in scene."
        return dependency_ls
    for d in dependency_dir_ls:
        # query the list of dependency files, attributes
        file_ls = pm.filePathEditor(q=True, listFiles=d, withAttribute=True, status=True)

        # groups the data in touples [(path, plug, status), ...]
        for path, plug, status in zip(file_ls[::3], file_ls[1::3], file_ls[2::3]):
            dep_info = DependencyInfo()
            dep_info.path = os.path.normpath(os.path.join(d, path))
            dep_info.plug = plug
            dep_info.status = int(status)

            # translate the type given by maya to a generic type and assign it to the Dependency item
            dep_type = pm.filePathEditor(plug, q=True, attributeType=1)
            ext = os.path.splitext(path)[-1].lower()
            if dep_type == "reference" and ext not in [".editma", ".editmb"]:
                dep_info.type = DependencyInfo.kReference
            elif dep_type == "file":
                dep_info.type = DependencyInfo.kTexture
            elif dep_type == "reference" and ext in [".editma", ".editmb"]:
                dep_info.type = DependencyInfo.kEdit
            else:
                dep_info.type = DependencyInfo.kUnknown

            dependency_ls.append(dep_info)
    return sorted(dependency_ls, key=lambda item: item.path)


def consolidateFileDependencies(dependencyList, targetPath):
    """
    Copy all the given dependencies to a desired target folder, and updates Maya's pointers.
    :param dependencyList: list of dependencies to be consolidated
    :param targetPath: folder where to copy the dependencies
    """
    # FIXME note that this function is not production ready, in that it doesn't try to prevent name clashes
    for dep in dependencyList:
        if not dep.type == dep.kTexture:
            continue
        if not os.path.exists(targetPath):
            os.makedirs(targetPath)
        name = os.path.basename(dep.path)
        shutil.copy2(dep.path, os.path.join(targetPath, name))
        pm.filePathEditor(dep.plug, repath=targetPath, force=True, recursive=1)
        pm.filePathEditor(refresh=True)

