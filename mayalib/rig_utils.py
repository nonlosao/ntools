import pymel.core as pm


def setUniqueBindPose(root):
    """
    Ensures a unique bind pose for the selected skeleton.
    Adapted from Christian Stejnar's setBindPose script:
    https://www.highend3d.com/maya/script/setbindpose-for-maya
    """

    if pm.nodeType(root) != "joint":
        pm.error("Object {} not of type 'joint'".format(root))

    # get old poses
    # listConnections can return duplicate nodes, which can lead to failure. set() fixes it.
    previous_poses = set(pm.listConnections(root_jnt, type="dagPose"))

    # delete old poses
    for old_pose in previous_poses:
        if old_pose.bindPose.get() == 1:
            pm.delete(old_pose)

    # create new pose
    new_bind_pose = pm.dagPose(root_jnt, save=1, g=1, name="bindPose")

    # define new pose as bindpose
    new_bind_pose.bindPose.set(1)

    print("New bindPose \"" + new_bind_pose + "\" created succesfully!\n")

