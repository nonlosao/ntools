# N-Tools

**This repository contains a bunch of scripts that I've written over the years - only those that can be made public**

Most of these scripts are small functions that I wrote to speed up my rigging workflow. Others are tests and experiment.



## The SDK Manager
I started working on this utility because I realized that Maya was missing a piece of UI where to monitor 
all the Set Driven Keys used in a scene at once.  
I wanted to have a similar interface to the Pose Editor, where you select a driver from a list 
and you can see all the poses connected to it.

![](mayalib/sdkMng/sdkManager_scrn1.png)

This tool is still a WIP, and there's still lots of work to do before it can be used in production.   
It is already possible to add and remove drivers, poses and driven channels, as well as perform basic editing on the curve
directly from the interface. However this functionality still requires improvements since not all the hotkeys people are 
familiar with are available in the Graph Editor panel yet.